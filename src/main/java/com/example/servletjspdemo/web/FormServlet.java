package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Conference form</h2>" +
				"<form action='data'>" +
					"First name: <input type='text' name='firstName' /> <br />" +
					"Surname: <input type='text' name='surname' /> <br />" +
					"Email: <input type='email' name='email' /> <br />" +
					"Confirm email: <input type='email' name='confirmEmail' /> <br />" +
					"Employer name: <input type='text' name='employerName' /> <br />" +
					"Where did you hear about this conference? <select>" +
						"<br /><option>Advertisement in work</option>" +
						"<option>Advertisement in school</option>" +
						"<option>Facebook</option>" +
						"<option>From friend</option>" +
						"<option>Other</option>" +
					"</select>" +
					"<br /> What are you doing for living? <input type='text' name='employerName' /> <br />" +
					"<input type='submit' value='SUBMIT' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}
