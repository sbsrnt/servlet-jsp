package com.example.servletjspdemo.service;

import java.util.ArrayList;
import java.util.List;

import com.example.servletjspdemo.domain.Person;

public class StorageService {
	
	private List<Person> db = new ArrayList<Person>();
	

	
	public void add(Person person){
		Person newPerson = new Person(person.getFirstName(), person.getSurname(), person.getEmail(), person.getConfirmEmail(), person.getEmployerName(), person.getWhere(), person.getJob(), person.getId());
		db.add(newPerson);
		person.id++; //daje id zeby mozna bylo zamknac formularz, w Person.java w konstruktorze jest id-- zeby nie bylo ciaglej inkrementacji id po odswiezeniu/dodaniu nowej osoby
 
		
	}
	
	public List<Person> getAllPersons(){
		return db;
	}

}
