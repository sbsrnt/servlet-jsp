package com.example.servletjspdemo.domain;

public class Person {
	
	public String firstName;
	public String surname;
	public String email;
	public String confirmEmail;
	public String employerName;
	public String where;
	public String job;
	public int id;
	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConfirmEmail() {
		return confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public String getWhere() {
		return where;
	}

	public void setWhere(String where) {
		this.where = where;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public Person() {
		super();
	}
	
	public Person(String firstName, String surname, String email, String confirmEmail, String employerName, String where, String job, int id) {
		super();
		this.firstName = firstName;
		this.surname = surname;
		this.email = email;
		this.confirmEmail = confirmEmail;
		this.employerName = employerName;
		this.where = where;
		this.job = job;
		this.id = id--;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
