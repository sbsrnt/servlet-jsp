<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<% Class.forName("sun.jdbc.odbc.JdbcOdbcDriver"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<jsp:useBean id="storage" class="com.example.servletjspdemo.service.StorageService" scope="application" />
<jsp:useBean id="person" class="com.example.servletjspdemo.domain.Person" scope="session" />

<form action="addPerson.jsp">

  First name :<input type="text" name="firstName" value="${person.firstName}" /><br />
  Surname :<input type="text"  name="surname" value="${person.surname}" /><br />
  Email :<input type="text"  name="email" value="${person.email}" /><br />
  Confirm email :<input type="text"  name="confirmEmail" value="${person.confirmEmail}" /><br />
  Employer name :<input type="text"  name="employerName" value="${person.employerName}" /><br />
  Where did you get info about this conference : <br />
	<input type="checkbox"  name="where" value="${person.where}" />Advertisement in work<br />
	<input type="checkbox"  name="where" value="${person.where}" />Advertisement in school<br />
	<input type="checkbox"  name="where" value="${person.where}" />Facebook<br />
	<input type="checkbox"  name="where" value="${person.where}" />From friend<br />
	<input type="checkbox"  name="where" value="${person.where}" />Other<br />
  What are you doing for a living :<textarea name="job"><% value="${person.job}"; %></textarea><br />
  <input type="submit" value=" SUBMIT ">
  <%
	if (person.id > 4) response.sendRedirect("full.jsp");
%>
<%
	  private HttpSession session;
	  private String message = " ";
	  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		
		
		if(session == null) {
			if(person.id <= 4) {
				if(person.getEmail().equals(person.getConfirmEmail())) {
					session = request.getSession(true);
			} else{
				response.sendRedirect("full.jsp");
			}
		} else message = "Already registered";
	
		doGet(request, response);
		}
  %>
</form>

</body>
</html>