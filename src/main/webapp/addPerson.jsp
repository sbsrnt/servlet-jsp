<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="person" class="com.example.servletjspdemo.domain.Person" scope="session" />

<jsp:setProperty name="person" property="*" /> 

<jsp:useBean id="storage" class="com.example.servletjspdemo.service.StorageService" scope="application" />

<% 
  storage.add(person);

%>

<p>Following person has been added to storage: </p>
<p>First name: ${person.firstName} </p>
<p>Surname: <jsp:getProperty name="person" property="surname"></jsp:getProperty></p>
<p>Email: <jsp:getProperty name="person" property="email"></jsp:getProperty></p>
<p>Confirm email: <jsp:getProperty name="person" property="confirmEmail"></jsp:getProperty></p>
<p>Where did you get info about conference: <jsp:getProperty name="person" property="where"></jsp:getProperty></p>
<p>What are you doing for a living: <jsp:getProperty name="person" property="job"></jsp:getProperty></p>
<p>
  <a href="showAllPersons.jsp">Show all persons</a>
</p>
</body>
</html>